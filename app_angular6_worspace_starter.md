# Criação do template app_angular6_workspace_starter

Template utilizando nx workspaces, com testes utilizando jest, formatação automática utilizando prettier,
edição online/remota utilizando theia, testes utilizando gauge, test runner jest visual. Documentação dos componentes Angular com Compodoc
E mais:

* Visualização de componentes das bibliotecas e dos aplicativos;
* Controle de estado da aplicação com NGRX
* Debug da aplicação com suporte a viagem no tempo



O workspace nwrl/nx permite modularizar a aplicação, além de possibilitar criar mais de uma app frontend angular
no mesmo repositório. É possível também gerar módulos que podem ser publicados separadamente em repositório npm.

Ref: https://github.com/nrwl/nx-examples#install-nx

```
nvm install 8.11.1 && \
npm i -g @angular/cli@6.0.1 && \
npm i -g @nrwl/schematics@next && \
create-nx-workspace angular6_workspace_starter --yarn && \
cd angular6_workspace_starter && \
ng g app web --style scss --routing true && \
ng g lib core && \
ng g lib state && \
ng g lib shared --routing true
```

## Adicionando hot module replacement `HMR`

```bash
yarn add -D @angularclass/hmr
```

Ver mudanças no commit de tag `hmr-habilitado`.

Ref: 

* https://github.com/angular/angular-cli/wiki/stories-application-environments
* https://github.com/angular/angular-cli/wiki/stories-configure-hmr

## Adicionar lib que será publicada separadamente

```bash
ng g lib nome_lib --publishable
```

## Adicionar Jest

### 1. Instalar dependências:


```bash
yarn add -D jest jest-preset-angular jest-spec-reporter
```

### outros reporters:

* https://github.com/Hargne/jest-html-reporter

* Jest Snapshots html reporter: https://github.com/guigrpa/jest-html

* Run Jest on Browser: https://www.npmjs.com/package/jest-browser ou https://blog.usejournal.com/getting-started-with-jest-and-puppeteer-7cf6c59a2cae


### 2. Adicionar no package.json:

```json
"scripts": {
    "test": "jest --watchAll",
    "test:ci": "jest --runInBand --coverage",
    ...
},
  "jest": {
    "globals": {
      "ts-jest": {
        "tsConfigFile": "tsconfig.spec.json"
      },
      "__TRANSFORM_HTML__": true
    },
    "reporters": ["jest-spec-reporter"],
    "collectCoverage": true,
    "coverageReporters": ["html"],
    "preset": "jest-preset-angular",
    "setupTestFrameworkScriptFile": "<rootDir>/jest/setupJest.ts"
  }
```

### 3. Adicionar arquivo `jest/setupJest.ts`, com o seguinte conteúdo:

```ts
import 'jest-preset-angular';
```

### Executar testes

```bash
yarn test
```


### Build de produção do Aplicativo Web SPA


### Execução do aplicativo compilado

```bash
npx spa-http-server dist/apps/web --push-state
```

## Adicionar Task para formatar automaticamente o código-fonte

Adicionar os seguintes pacotes de dependências:

```bash
yarn add -D prettier lint-staged husky tslint-config-prettier listr@0.14
```

#### Por alguma razão, o lint-staged não funciona sem adicionar a dependência rxjs-compat:

```bash
yarn add rxjs-compat
```

### Adicionar as configurações de lint-staged e o script de precommit do husky.

Adicionar em scripts:

```json
  "scripts": {
    ...
     "prettier-format": "prettier {apps,tools,libs,xplat,e2e}/**/*.{ts,tsx,json,md,scss,css,less} --write",
     "precommit": "lint-staged"
  },
  "lint-staged": {
    "*.{ts,js,json,css,md}": ["yarn prettier-format", "git add"]
  }
```

No arquivo tslint.json, adicione a seguinte entrada: 

```json
  "extends": [
    "tslint-config-prettier"
  ],
```

Agora execute o seguinet comando, para verificar se sua configuração de tslint possui algum conflito com o prettier.

Caso o único conflito seja o no-trailing-whitespace, você pode ficar tranquilo, pois o VSCode já trata de remover os espaços em branco adicionais em finais de linha automaticamente ao salvar.

Agora, ao comitar arquivos, eles serão formatados automaticamente antes de confirmar o commit, facilitando manter uma padronização de código do seu projeto.

## Documentação com Compodoc


```bash
yarn add -D @compodoc/compodoc
```

### Adicione no package.json

```json
  "scripts": {
    ...
    "compodoc": "compodoc -p tsconfig.json -s"
  }
```

### Gerando a documentação

```bash
yarn compodoc
```

Tab com live do componente:

https://compodoc.github.io/website/guides/live-example-tab.html


```js
 /**
  * Example of usage:
  * <example-url>http://localhost/demo/mysample.component.html</example-url>
  * <example-url>/demo/mysample.component.html</example-url>
  */
```


### Adicionando StoryBook

```bash
npm i -g @storybook/cli@4.0.0-alpha.4; yarn add -D react react-dom && \
getstorybook && \
yarn add -D @storybook/addon-jest
```

Adicione o script `storybook` no package.json:


```json
   "scripts": {
     ...
     "jest-for-storybook": "jest --json --outputFile=./storybook/jest-test-results.json",
     "storybook": "start-storybook -p 9001 -c .storybook",
   }


### Para ter o resultado dos testes visíveis em live-reload no storybook, execute:

```bash
npm run jest-for-storybook -- --watch
```

### Executar o storybook:

```bash
yarn storybook
```

### StoryBook - Roadmap

* Adicionar o addon de knobs para permitir usuário interagir com os componentes https://github.com/storybooks/storybook/tree/master/addons/knobs
 
 - Ref: https://storybooks-angular.netlify.com/?knob-phoneNumber=555-55-55&knob-title=Meu%20t%C3%ADtulo&knob-name=Jane&knob-field=abner&knob-border=deeppink&knob-text=Titulo%20do%20bot%C3%A3o&knob-Rendered%20string=%3Cimg%20src%3Dx%20onerror%3D%22alert%28%27XSS%20Attack%27%29%22%20%3E&knob-age=0&knob-items%5B0%5D=Laptop&knob-items%5B1%5D=Book&knob-items%5B2%5D=Whiskey&knob-today=1484881200000&knob-price=2.25&knob-nice=true&knob-fruit=apple&knob-stock=20&
 selectedKind=Addon%7CKnobs&selectedStory=All%20knobs&full=0&addons=1&stories=1&panelRight=0&addonPanel=storybooks%2Fstorybook-addon-knobs

* Adicionar o addon  story-source: https://github.com/storybooks/storybook/tree/master/addons/storysource

* Addon para centralizar componentes https://github.com/storybooks/storybook/tree/master/addons/centered

* Criar addon para exibir formulários com DoubleAgentValidator

* Adicionar addon de anotações: https://github.com/storybooks/storybook/tree/master/addons/notes

REF: 

* Addon API https://storybook.js.org/addons/api/

* https://storybook.js.org/addons/introduction/

* https://storybook.js.org/examples/

* https://building.coursera.org/ui/?selectedKind=extended.Notification&selectedStory=Simple%20usage&full=0&down=0&left=1&panelRight=1&downPanel=tuchk4%2Freadme%2Fpanel

* TimeLine Component and more: developers.fyndiq.com/fyndiq-ui/?selectedKind=Timeline&selectedStory=default&full=0&addons=1&stories=1a&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel

* Coursera StoryBook https://building.coursera.org/coursera-ui/?selectedKind=basic.Gradient&selectedStory=GradientSection&full=0&addons=0&stories=1&panelRight=0&addonPanel=storybook%2Factions%2Factions-panel



## Playground

O playground permite criar ambientes isolados para testar os componentes.

Como instalar com o Angular 6:

```bash

yarn add -D angular-playground concurrently

```

Adicionar a seguinte entrada no angular.json: na chave `projects`

```json

"web-playground": {
      "root": "apps/web",
      "sourceRoot": "apps/web/src",
      "projectType": "application",
      "architect": {
        "build": {
          "builder": "@angular-devkit/build-angular:browser",
          "options": {
            "outputPath": "dist/apps/web-playground",
            "index": "apps/web/src/index.html",
            "main": "apps/web/src/main.playground.ts",
            "polyfills": "apps/web/src/polyfills.ts",
            "tsConfig": "apps/web/tsconfig.app.json",
            "assets": [
              "apps/web/src/favicon.ico",
              "apps/web/src/assets"
            ],
            "styles": [
              "apps/web/src/styles.scss"
            ],
            "scripts": []
          },
          "configurations": {
            "hmr": {
              "fileReplacements": [
                {
                  "replace": "apps/web/src/environments/environment.ts",
                  "with": "apps/web/src/environments/environment.hmr.ts"
                }
              ]
            },
            "production": {
              "fileReplacements": [
                {
                  "replace": "apps/web/src/environments/environment.ts",
                  "with": "apps/web/src/environments/environment.prod.ts"
                }
              ],
              "optimization": true,
              "outputHashing": "all",
              "sourceMap": false,
              "extractCss": true,
              "namedChunks": false,
              "aot": true,
              "extractLicenses": true,
              "vendorChunk": false,
              "buildOptimizer": true
            }
          }
        },
        "serve": {
          "builder": "@angular-devkit/build-angular:dev-server",
          "options": {
            "port": 4201,
            "browserTarget": "web-playground:build"
          },
          "configurations": {
            "production": {
              "browserTarget": "web-playground:build:production"
            },
            "hmr": {
              "browserTarget": "web-playground:build:hmr",
              "hmr": true,
              "hmrWarning": false
            }
          }
        },
        "extract-i18n": {
          "builder": "@angular-devkit/build-angular:extract-i18n",
          "options": {
            "browserTarget": "web-playgroud:build"
          }
        },
        "test": {
          "builder": "@angular-devkit/build-angular:karma",
          "options": {
            "main": "apps/web/src/test.ts",
            "polyfills": "apps/web/src/polyfills.ts",
            "tsConfig": "apps/web/tsconfig.spec.json",
            "karmaConfig": "apps/web/karma.conf.js",
            "styles": [
              "apps/web/styles.scss"
            ],
            "scripts": [],
            "assets": [
              "apps/web/src/favicon.ico",
              "apps/web/src/assets"
            ]
          }
        },
        "lint": {
          "builder": "@angular-devkit/build-angular:tslint",
          "options": {
            "tsConfig": [
              "apps/web/tsconfig.app.json",
              "apps/web/tsconfig.spec.json"
            ],
            "exclude": [
              "**/node_modules/**"
            ]
          }
        }
      }
    },

```

Adicionar o script a seguir no package.json:

```json

"scripts": {
  ...
  "playground-web": "concurrently \"ng serve web-playground\" \"angular-playground --no-serve\""
}

```

Adicionar o arquivo `angular-playground.json` com o seguinte conteúdo:


```json

{
  "sourceRoot": "./apps/web/src",
  "reportPath": "./sandbox.report.json",
  "reportType": "log"
}


Executando o playground:


```bash

yarn web-playground

```

O playground estará em execução na porta 4201. Acesse o endereço http://localhost:4201 no seu navegador e com o comando 
`Ctrl + P` ou `F2` você pode alternar entre os sandboxes definidos no projeto.


Para alternar dinâmicamente entre sandboxes você pode utilizar as teclas de atalho `ctrl + cima` ou `ctrl + baixo` quando o navegador de componentes estiver visível.


É possível expor o sandbox embutido em uma outra página, adicionando o parâmetro embed=l, o que faz com que o código da barra de comando não seja adicionado na página, e também, apenas o código necessário para o sandbox específico é adicionado.

Exemplo: `http://localhost:4201/?scenario=.%2Fapp%2Fsimple.component.sandbox/with%20color%20blue&embed=0`


### Exemplo de sandbox: (arquivo simple.component.sandbox.ts):

```ts

import { sandboxOf } from 'angular-playground';
import { SimpleComponent } from './simple.component';

// TODO: remover os arquivos de sandbox da cobertura do JEST
export default sandboxOf(SimpleComponent,
  /* É possível passar declarations, imports, providers aqui
  neste segundo objeto */{

  })
  .add('with color red', {
    template: `<simple-component [color]="cor">Hey playground!</simple-component>`,
    context: {
      cor: 'red'
    }
  }).add('with color blue', {
    template: `<simple-component color='blue'>Hey playground!</simple-component>`,
    context: {
      cor:'blue'
    }
  });

```


## Execução de testes funcionais com gauge


Outras referências:

* https://medium.com/@martin_hotell/use-react-tools-for-better-angular-apps-b0f14f3f8114
