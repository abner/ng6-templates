import { storiesOf, addDecorator } from '@storybook/angular';
import { withNotes } from '@storybook/addon-notes';
import { action } from '@storybook/addon-actions';
import { linkTo } from '@storybook/addon-links';
import { withTests } from '@storybook/addon-jest';
import { withCompodoc } from 'storybook-addon-compodoc';

import { Welcome, Button } from '@storybook/angular/demo';
import { SimpleComponent } from '../../apps/web/src/app/simple.component';

//const results = require('../../.storybook/jest-test-results.json');
let results: any;

try {
  results = require('../../.storybook/jest-test-results.json');
} catch (e) {
  results = require('/tmp/jest_runner.json');
}

declare var module: any;

const filesExt = '((\\.specs?)|(\\.tests?))?(\\.ts)?$';

storiesOf('Welcome', module).add('to Storybook', () => ({
  component: Welcome,
  props: {}
}));

storiesOf('SimpleComponent', module)
  .addDecorator(
    withCompodoc(
      () => ({
        component: SimpleComponent,
        props: {
          color: 'darkgray'
        }
      }),
      { compodocUrl: 'http://localhost:4202' }
    )
  )
  .addDecorator(withTests({ results, filesExt })('simple.component'))
  .add('simple', () => null);

storiesOf('Button', module)
  .addDecorator(withTests({ results })('AppComponent', 'SharedModule'))
  .add('with text', () => ({
    component: Button,
    props: {
      text: 'Hello Button'
    }
  }))
  .add(
    'with some emoji',
    withNotes({ text: 'My notes on a button with emojis' })(() => ({
      component: Button,
      props: {
        text: '😀 😎 👍 💯'
      }
    }))
  )
  .add(
    'with some emoji and action',
    withNotes({ text: 'My notes on a button with emojis' })(() => ({
      component: Button,
      props: {
        text: '😀 😎 👍 💯',
        onClick: action('This was clicked OMG')
      }
    }))
  );

storiesOf('Another Button', module).add(
  'button with link to another story',
  () => ({
    component: Button,
    props: {
      text: 'Go to Welcome Story',
      onClick: linkTo('Welcome')
    }
  })
);
