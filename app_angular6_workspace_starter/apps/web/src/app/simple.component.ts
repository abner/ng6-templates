import { Component, Input } from '@angular/core';

@Component({
  selector: 'simple-component',
  template: '<h1 [style.color]="color">My Simple Component {{ color }} <h1>',
  styles: ['h1 { color: #036 }']
})
export class SimpleComponent {
  @Input() color = 'blue';
}
