import { sandboxOf } from 'angular-playground';
import { SimpleComponent } from './simple.component';

// TODO: remover os arquivos de sandbox da cobertura do JEST
export default sandboxOf(
  SimpleComponent,
  /* É possível passar declarations, imports, providers aqui
  neste segundo objeto */ {}
)
  .add('with color red', {
    template: `<simple-component [color]="cor">Hey playground!</simple-component>`,
    context: {
      cor: 'red'
    }
  })
  .add('with color blue', {
    template: `<simple-component color='blue'>Hey playground!</simple-component>`,
    context: {
      cor: 'blue'
    }
  });
