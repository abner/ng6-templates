import { SimpleComponent } from './simple.component';

describe('SimpleComponent', () => {
  it('can be instantiated', () => {
    const cmp = new SimpleComponent();
    expect(cmp).toBeDefined();
  });
});
